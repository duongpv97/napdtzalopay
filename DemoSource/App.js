import React from 'react';
import { 
  Button, 
  View, 
  Text,
  TextInput,
  Platform,
  StyleSheet,
  TouchableOpacity,
  Image, 
} from 'react-native';
import { createStackNavigator } from 'react-navigation';

const assets = {
  'ico_card': require('./Asset/ico_card.png'),
  'ico_contact': require('./Asset/ico_contact.png'),
  'ico_electric': require('./Asset/ico_electric.png'),
  'ico_internet': require('./Asset/ico_internet.png'),
  'ico_smartphone': require('./Asset/ico_smartphone.png'),
  'ico_water': require('./Asset/ico_water.png'),
}

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.styleHome}>
        <Button
          title="Nạp ĐT"
          onPress={() => this.props.navigation.navigate('NapDT')}
        />
      </View>
    );
  }
}



class LogoTitle extends React.Component {
  render(){
    return (
      <View style={styles.ViewRow}>
        <View style={styles.logoHeader}>
          <Text style={styles.TextHeader}>Nạp Tiền Điện Thoại</Text> 
        </View>
        <View style={styles.imgHeader}>
          <ButtonImage imgName='ico_card' />
        </View>
      </View>   
    );
  }
}

class ButtonDoubleText extends React.Component {
  render () {
    return (
      <TouchableOpacity onPress={()=>{alert('button is clicked')}}>
        <View style={styles.ViewCell}>
          <Text style={styles.TextBoldBlack}>{this.props.value1}</Text>
          <Text style={styles.TextGrey}>{this.props.value2}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class ButtonImage extends React.Component {
  render(){
    return (
      <TouchableOpacity onPress={()=>{alert('button is clicked')}}>
        <View style={styles.AlignCenter}>
          <Image style={styles.ImageSmall} source={assets[this.props.imgName]} />
        </View>
      </TouchableOpacity>
    );
  }
}

class ButtonImageTextLarge extends React.Component {
  render(){
    return (
      <TouchableOpacity onPress={()=>{alert('button is clicked')}}>
        <View style={styles.AlignCenter}>
          <Image style={styles.ImageLarge} source={assets[this.props.imgName]} />
          <Text style={styles.TextButton}>{this.props.name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class NapDTScreen extends React.Component {
  static navigationOptions= {
    headerTitle: <LogoTitle />,
    headerStyle:{
      backgroundColor: 'steelblue'
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color: '#fff',
    },
    headerTintColor: '#fff',
    animationEnabled: true
  };

  constructor(props) {
    super(props);
    this.state = { text: '0919670338' };
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <View style={styles.ViewRow}>
            <View style={styles.textPhone}>
              <Text style={styles.textGreySmall}>Số của tôi - Vinaphone</Text>          
              <TextInput  style={styles.textBlackLarge} 
                          underlineColorAndroid='transparent'
                          keyboardType='phone-pad'
                          maxLength={10}
                          onChangeText={(text) => this.setState({text})} 
                          value={this.state.text} />
            </View>
            <View style={styles.imgHeader}>
              <ButtonImage imgName='ico_contact' />
            </View>
          </View>
        </View>

        <View style={styles.body}>
          <View style={styles.ViewRow}>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='10k' value2='9.530'/>
            </View>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='20k' value2='19.060'/>
            </View>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='50k' value2='47.650'/>
            </View>
          </View>
          <View style={styles.ViewRow}>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='100k' value2='95.300'/>
            </View>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='200k' value2='190.600'/>
            </View>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='300k' value2='285.900'/>
            </View>
          </View>
          <View style={styles.ViewRow}>
            <View style={[styles.ViewCell, styles.Border, styles.CellMargin]}>
              <ButtonDoubleText value1='500k' value2='476.500'/>
            </View>
            <View style={[styles.ViewCell, styles.CellMargin]}>
            </View>
            <View style={[styles.ViewCell, styles.CellMargin]}>
            </View>
          </View>
        </View>

        <View style={styles.footer}>
          <View style={{flex: 0.5}}>
            <Text style={styles.textInstruction}>DỊCH VỤ KHÁC</Text>
          </View>
          <View style={styles.ViewRow}>
            <View style={styles.ViewCell}>
              <ButtonImageTextLarge imgName='ico_smartphone' name='Thẻ ĐT' />
            </View>
            <View style={styles.ViewCell}>
              <ButtonImageTextLarge imgName='ico_electric' name='Điện' />
            </View>
            <View style={styles.ViewCell}>
              <ButtonImageTextLarge imgName='ico_water' name='Nước' />
            </View>
            <View style={styles.ViewCell}>
              <ButtonImageTextLarge imgName='ico_internet' name='Internet' />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    NapDT: NapDTScreen,
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header:{
    flex: 0.5,
    borderBottomWidth: 1,
    borderBottomColor: '#b6b8ba',
  },
  body:{
    flex: 3,
  },
  footer:{
    flex:1,
  },
  ViewRow:{
    flex:1,
    flexDirection: 'row',
  },
  ViewCell:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  ImageLarge:{
    width:50,
    height:50,
  },
  AlignCenter:{
    justifyContent: 'center',
    alignItems:'center',
  },
  TextButton:{
    fontFamily: 'Arial',
    fontSize: 15,
  },
  TextBoldBlack: {
    fontFamily: 'Arial',
    fontSize: 20,
    fontWeight: 'bold'
  },
  TextGrey: {
    color: '#5d5f60',
    fontFamily: 'Arial',
  },
  Border:{
    borderWidth: 1,
    borderColor: '#b6b8ba'
  },
  CellMargin:{
    margin: 5,
  },
  TextHeader:{
    color:'#ffffff',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: 20,
  },
  ImageSmall:{
    width:25,
    height:25,
  },
  logoHeader:{
    flex: 4, 
    justifyContent: 'center'
  },
  imgHeader:{
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  styleHome:{ 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
   },
  textGreySmall:{
    paddingLeft: 5, 
    fontFamily: 'Arial', 
    fontSize: 12, 
    color:'grey'
  },
  textBlackLarge:{
    paddingLeft: 5, 
    fontFamily: 'Arial', 
    fontSize: 20, 
    color:'black'
  },
  textPhone:{
    flex:4, 
    justifyContent: 
    'center'
  },
  textInstruction:{
    fontFamily: 'Arial', 
    fontSize: 17, 
    color: 'grey', 
    paddingTop: 10, 
    paddingLeft: 5
  },
});


